import { IsNotEmpty, IsPositive, MaxLength } from 'class-validator';

export class CreateCustomerDto {
  id: number;

  @IsNotEmpty()
  @MaxLength(50)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  age: number;

  @IsNotEmpty()
  @MaxLength(10)
  tel: string;

  @IsNotEmpty()
  gender: string;
}
