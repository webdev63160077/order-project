import { IsNotEmpty, IsPositive, MaxLength } from 'class-validator';

export class CreateProductDto {
  id: number;

  @IsNotEmpty()
  @MaxLength(50)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  price: number;
}
