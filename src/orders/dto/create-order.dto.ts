import { IsPositive } from 'class-validator';

class CreateOrderItemDto {
  productId: number;

  @IsPositive()
  amount: number;
}

export class CreateOrderDto {
  customerId: number;
  orderItems: CreateOrderItemDto[];
}
